﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NosebleedStudios.GameFramework
{
    [Serializable]
    public class Content: ISerializable, IDeserializationCallback, ICloneable
    {
        public string Id { get; set; }

        public Content() { 
        
        }

        public Content(SerializationInfo info, StreamingContext context) {
            Id = info.GetString("id");
        }

        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("id", Id);
        }

        public virtual void OnDeserialization(object sender)
        {
            
        }

        public object Clone()
        {
            return CloneObject();
        }

        protected virtual object CloneObject()
        {
            return MemberwiseClone();
        }
    }
}
