﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NosebleedStudios.GameFramework
{
    [Serializable]
    public abstract class DrawableComponent: Component
    {
        string _layerName;

        public DrawableComponent(string layerName) {
            _layerName = layerName;
        }


        public DrawLayer Layer { get { return _layer; } }

        internal LinkedListNode<DrawableComponent> DrawNode;
        [NonSerialized]
        private DrawLayer _layer;

        protected internal override void Start()
        {
            base.Start();
            _layer = Scene.Layers[_layerName];
            _layer.AddDrawable(this);
        }

        protected internal override void Stop()
        {
            Layer.RemoveDrawable(this);
        }

        internal void Render() {
            if (Enabled) Draw();
        }

        protected abstract void Draw();
    }
}
