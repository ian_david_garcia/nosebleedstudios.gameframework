﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NosebleedStudios.GameFramework
{
    public class ContentManager: IDisposable
    {
        private Dictionary<string, object> _items = new Dictionary<string, object>();
        private bool _isDisposed=false;
        public Dictionary<string, object> Items { get { return _items; } }

        public void Unload(string id, bool dispose=false)
        {
            var item = _items[id];
            _items.Remove(id);
            if (dispose && item is IDisposable) ((IDisposable)item).Dispose();
        }

        public void UnloadAll(bool dispose=false) {
            if (dispose)
            {
                foreach (var item in _items)
                {
                    if (item.Value is IDisposable) ((IDisposable)item.Value).Dispose();
                }
            }
            _items.Clear();
        }

        public virtual void Dispose() {
            if (!_isDisposed) {
                _isDisposed = true;
                UnloadAll();
                GC.SuppressFinalize(this);
            }
        }

        public virtual void Add(string id, object obj) {
            if (!_items.ContainsKey(id))
                _items[id] = obj;
        }

        public virtual T Load<T>(string id)
        {
            if(!_items.ContainsKey(id)) {
                var itm = Loaders[typeof(T)](id, id);
                _items[id] = itm;
                Console.WriteLine("Loaded resource: " + id);
                return (T)itm;
            }
            return (T)_items[id];
        }

        public T Load<T>(string id, string name)
        {
            if (!_items.ContainsKey(id))
            {
                var itm = Loaders[typeof(T)](id, name);
                _items[id] = itm;
                Console.WriteLine("Loaded resource: {0} : {1}", id, name);
                return (T)itm;
            }
            return (T)_items[id];
        }

        public IEnumerable<T> GetAllOfType<T>() {
            return _items.Select(s => s.Value).OfType<T>();
        }

        public static Dictionary<Type, Func<string, string, object>> Loaders { get { return _loaders; } }
        static readonly Dictionary<Type, Func<string, string, object>> _loaders = new Dictionary<Type, Func<string, string, object>>();
    }
}
