﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NosebleedStudios.GameFramework
{
    [Serializable]
    public class UpdateableComponent: Component
    {
        protected internal override void Start()
        {
            Entity.Scene.RegisterUpdateable(this);
        }

        protected internal override void Stop()
        {
            MarkedForRemoval = true;
        }

        protected internal virtual void Update(TimeSpan deltaTime) { }

        internal LinkedListNode<UpdateableComponent> _updateNode;
        internal bool MarkedForRemoval=false;
    }
}
