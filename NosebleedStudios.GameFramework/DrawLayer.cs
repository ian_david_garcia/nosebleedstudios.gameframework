﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NosebleedStudios.GameFramework
{
    public class DrawLayer
    {
        public string Name { get; private set; }
        public bool IsVisible { get; set; }

        LinkedList<DrawableComponent> _objects = new LinkedList<DrawableComponent>();

        public IEnumerable<DrawableComponent> Objects { get { return _objects; } }

        public DrawLayer(string name)
        {
            IsVisible = true;
            Name = name;
            Index = -1;
        }

        internal void AddDrawable(DrawableComponent c) {
            if (c.DrawNode != null) c.DrawNode.List.Remove(c.DrawNode);
            c.DrawNode = _objects.AddLast(c);
        }

        internal void RemoveDrawable(DrawableComponent c)
        {
            _objects.Remove(c.DrawNode);
        }

        protected internal virtual void Render(TimeSpan deltaTime)
        {
            foreach (var item in _objects)
                item.Render();
        }

        public int Index { get; internal set; }

        public Scene Scene { get; internal set; }
    }
}
