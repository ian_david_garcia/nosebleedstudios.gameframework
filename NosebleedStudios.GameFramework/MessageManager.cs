﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NosebleedStudios.GameFramework
{
    public delegate void MessageListener(object source, int messageId, Message messageArgs);

    public class MessageManager
    {
        MultiValueDictionary<int, MessageListener> _handlers = new MultiValueDictionary<int, MessageListener>();
        public void AddMessageListener(int message, MessageListener listener)
        {
            _handlers.Add(message, listener);
        }

        public void RemoveMessageListener(int message, MessageListener listener)
        {
            _handlers.Remove(message, listener);
        }

        public void SendMessage(object source, int messageId, Message messageData=null)
        {
            HashSet<MessageListener> listeners=null;
            if (_handlers.TryGetValue(messageId, out listeners))
                foreach (var l in listeners)
                    l(source, messageId, messageData);
        }
    }
}
