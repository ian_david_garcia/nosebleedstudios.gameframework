﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NosebleedStudios.GameFramework
{
    [Serializable]
    public class Scene: IDisposable, System.Runtime.Serialization.IDeserializationCallback
    {
        public class LayerCollection: IEnumerable<DrawLayer> {

            internal Scene Scene;
            
            internal readonly List<DrawLayer> LayersByIndex = new List<DrawLayer>();
            internal readonly Dictionary<string, DrawLayer> LayersByName = new Dictionary<string,DrawLayer>();

            public DrawLayer this[int index]{
                get { return LayersByIndex[index]; }
            }

            public DrawLayer this[string index]
            {
                get { return LayersByName[index]; }
            }

            public void Add(DrawLayer l) {
                if (LayersByIndex.Contains(l) || LayersByName.ContainsValue(l)) throw new InvalidOperationException("Layer already exists inside scene.");
                if (LayersByName.ContainsKey(l.Name))  throw new InvalidOperationException("A layer with that name already exists.");
                LayersByIndex.Add(l);
                l.Index = LayersByIndex.Count - 1;
                LayersByName[l.Name] = l;
                l.Scene = Scene;
            }

            public void Remove(DrawLayer l)
            {
                if (l.Index >= LayersByIndex.Count || !LayersByIndex.Contains(l) || LayersByIndex[l.Index] != l) throw new InvalidOperationException("Layer does not exist in scene.");
                LayersByIndex.RemoveAt(l.Index);
                LayersByName.Remove(l.Name);
                l.Index = -1;
                l.Scene = null;
            }

            public IEnumerator<DrawLayer> GetEnumerator()
            {
                return LayersByIndex.GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return ((IEnumerable)LayersByIndex).GetEnumerator();
            }

            public void AddRange(params DrawLayer[] layers)
            {
                foreach (var l in layers)
                    Add(l);
            }
        }

        /// <summary>
        /// The root entity.
        /// </summary>
        public Entity Root { get; private set; }

        public SceneManager Manager { 
            get { return _manager; }
            internal set { _manager = value; }
        }
        public bool IsCurrent { get { return Manager == null ? false : Manager.Current == this; } }


        public LayerCollection Layers { get { return _layers; } }

        /// <summary>
        /// The entities contained in the root entity.
        /// </summary>
        public IEnumerable<Entity> Entities { get { return Root.Children; } }

        [NonSerialized]
        LayerCollection _layers;
        
        private LinkedList<UpdateableComponent> _updateables = new LinkedList<UpdateableComponent>();

        [NonSerialized]
        private CoroutineManager _beforeUpdateCoroutines = new CoroutineManager();
        [NonSerialized]
        private CoroutineManager _afterUpdateCoroutines = new CoroutineManager();
        [NonSerialized]
        private SceneManager _manager;
        /// <summary>
        /// Construct the scene.
        /// </summary>
        /// <param name="name">The scene name.</param>
        public Scene()
        {
            Name = GetType().Name;
            Root = new Entity("Root");
            Root.Scene = this;

            Root.Start();
            _layers = new LayerCollection() { Scene = this };
        }

        /// <summary>
        /// Add an entity to the root object.
        /// </summary>
        /// <param name="entity">The entity to add.</param>
        public void AddEntity(params Entity[] entity)
        {
            Root.AddChild(entity);
        }

        /// <summary>
        /// Remove an entity from the scene. Entity must be a a direct child of the root entity.
        /// </summary>
        /// <param name="entity">The entity to remove.</param>
        public void RemoveEntity(Entity entity)
        {
            Root.RemoveChild(entity);
        }

        /// <summary>
        /// Find an entity by name.
        /// </summary>
        /// <param name="name">Name of the entity.</param>
        /// <returns>The entity. Returns null if not found.</returns>
        public Entity FindEntity(string name) {
            return Root.FindChild(name);
        }

        /// <summary>
        /// Send a message to the root object and its children
        /// </summary>
        /// <param name="messageId">An arbitrary string that identifies the message.</param>
        /// <param name="includeChildren">If true, send to all children, not just the root and its direct children.</param>
        /// <param name="args">An optional parameter that contains the message details.</param>
        public void SendMessage(object source, int messageId, Message args=null) {
            Root.SendMessage(source, messageId, args);
        }

        public void BroadcastMessage(object source, int messageId, Message args=null)
        {
            Root.BroadcastMessage(source, messageId, args);
        }

        /// <summary>
        /// Updates the scene.
        /// </summary>
        /// <param name="deltaTime">How much time elapsed since the last frame.</param>
        public void Update(TimeSpan deltaTime) {
            _beforeUpdateCoroutines.RunCoroutines(deltaTime);

            var node = _updateables.First;
            while (node != null)
            {
                var thisNode = node;
                node = node.Next;
                if (thisNode.Value.MarkedForRemoval)
                {
                    _updateables.Remove(thisNode);
                    continue;
                }
                thisNode.Value.Update(deltaTime);
            }
            _afterUpdateCoroutines.RunCoroutines(deltaTime);
        }

        /// <summary>
        /// Start a coroutine. Use by calling the coroutine and passing the return value to this function.
        /// </summary>
        /// <example>StartCoroutine(Update())</example>
        /// <param name="routine">The routine to add.</param>
        /// <param name="beforeUpdate">By default, coroutines execute after components are updated. Set this to true
        /// to run the routine before components are updated.</param>
        public void StartCoroutine(IEnumerable routine, bool beforeUpdate=false) {
            if (beforeUpdate) _beforeUpdateCoroutines.StartCoroutine(routine);
            else _afterUpdateCoroutines.StartCoroutine(routine);
        }

        internal void RegisterUpdateable(UpdateableComponent comp)
        {
            if (comp._updateNode != null)
                throw new ArgumentException("Cannot register UpdateableComponent - node not null.");
            comp._updateNode = _updateables.AddLast(comp);
        }

        public virtual void Render(TimeSpan deltaTime) {
            foreach (var layer in Layers) {
                layer.Render(deltaTime);
            }
        }

        private bool _isDisposed = false;

        public void Dispose() {
            if (!_isDisposed) {
                _isDisposed = true;
                Destroy();
                GC.SuppressFinalize(this);
            }
        }

        protected virtual void Destroy()
        {
        }

        public virtual void OnDeserialization(object sender)
        {
            _beforeUpdateCoroutines = new CoroutineManager();
            _afterUpdateCoroutines = new CoroutineManager();
            _layers = new LayerCollection();
        }

        public string Name { get; protected set; }

        protected internal virtual void Activate()
        {
        }

        protected internal virtual void Deactivate()
        {
        }

        internal void DoActivate()
        {
            Activate();
            BroadcastMessage(this, Messages.Framework.SceneActivated);
        }

        internal void DoDeactivate()
        {
            Deactivate();
            BroadcastMessage(this, Messages.Framework.SceneDeactivated);
        }
    }
}
