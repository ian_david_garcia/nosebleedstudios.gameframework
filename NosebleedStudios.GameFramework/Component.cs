﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NosebleedStudios.GameFramework
{
    [Serializable]
    public abstract class Component : IDisposable
    {
        [NonSerialized]
        private bool _isDisposed = false;
        public bool IsStarted { get { return Entity != null && Entity.IsStarted; } }
        internal LinkedListNode<Component> _componentNode;

        public bool Enabled { get; set; }

        public Component() {
            Enabled = true;
        }

        public Scene Scene {
            get { return Entity != null ? Entity.Scene : null; } 
        }

        protected internal virtual void Start()
        {
        }

        protected internal virtual void Stop()
        {
        }

        public T GetComponent<T>() {
            if (Entity == null) return default(T);
            return Entity.GetComponent<T>();
        }

        public IEnumerable<T> GetComponents<T>(bool includeChildren=false)
        {
            if (Entity == null) return null;
            return Entity.GetComponents<T>(includeChildren);
        }

        public void RemoveFromEntity() {
            if (_componentNode == null || Entity==null || _componentNode.List!=Entity._components) throw new InvalidOperationException();
            Entity._components.Remove(_componentNode);
        }

        public void Dispose()
        {
            if (!_isDisposed)
            {
                Destroy();
                GC.SuppressFinalize(this);
                _isDisposed = true;
            }
        }

        protected void Destroy() { }

        public Entity Entity { get; internal set; }

        public T RequireComponent<T>(Entity e=null)
        {
            var cmp = e==null ? GetComponent<T>() : e.GetComponent<T>();
            if (cmp == null) throw new RequireComponentException(this, typeof(T));
            return cmp;
        }
    }

    public class RequireComponentException : Exception {
        public Component Component { get; private set; }
        public Type RequiredComponentType { get; private set; }

        public override string Message
        {
            get
            {
                return string.Format("Component {0} requires component of type {1} in entity {2}", 
                    Component.GetType().FullName,
                    RequiredComponentType.FullName,
                    Component.Entity != null ? Component.Entity.Name : "<null>"
                    );
            }
        }

        public RequireComponentException(Component component, Type requiredComponentType) {
            Component = component;
            RequiredComponentType = requiredComponentType;
        }
    }
}
