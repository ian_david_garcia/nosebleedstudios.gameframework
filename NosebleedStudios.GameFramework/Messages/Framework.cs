﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NosebleedStudios.GameFramework.Messages
{
    public static class Framework
    {
        public const int MessageIdStart = -100000;
        public const int ChildAdded = MessageIdStart - 1;
        public const int ChildRemoved = MessageIdStart - 2;
        public const int SceneActivated = MessageIdStart - 3;
        public const int SceneDeactivated = MessageIdStart - 4;
    }
}
