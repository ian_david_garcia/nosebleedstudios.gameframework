﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace NosebleedStudios.GameFramework
{

    [Serializable]
    public class Message
    {
        public static Message CreateDataMessage<T>(T data)
        {
            return new DataMessage<T>(data);
        }
        public T AsDataMessage<T>()
        {
            if (!(this is DataMessage<T>)) throw new InvalidCastException( string.Format("Message is not a DataMessage<{0}>.", typeof(T).Name));
            return ((DataMessage<T>)this).Data;
        }
    }
    public class DataMessage<T>: Message
    {
        public T Data { get; set; }
        public DataMessage(T data)
        {
            Data = data;
        }
        public static DataMessage<T> Create(T obj)
        {
            return new DataMessage<T>(obj);
        }

        public override string ToString()
        {
            return string.Format("DataMessage<{0}>",typeof(T).Name);
        }
    }

    public static class MessageUtils {
        public static string GetConstName<T>(int value) { 
            FieldInfo[] thisObjectProperties = typeof(T).GetFields();
            foreach (FieldInfo info in thisObjectProperties) {
                if (info.IsLiteral && info.GetRawConstantValue().Equals(value)) {
                    return info.Name;
                }
            }
            return null;
        }
    }
}
