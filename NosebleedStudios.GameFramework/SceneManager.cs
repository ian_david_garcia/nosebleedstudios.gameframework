﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NosebleedStudios.GameFramework
{
    public class SceneManager
    {
        public Scene Current { get; private set; }
        public IReadOnlyList<Scene> Scenes { get { return _scenes; } }

        List<Scene> _scenes = new List<Scene>();

        public void Add(Scene scene)
        {
            if (scene == null) throw new ArgumentNullException("Scene is null.");
            if (_scenes.Contains(scene)) throw new ArgumentException("Scene already exists.");
            _scenes.Add(scene);
            scene.Manager = this;
        }

        public void MakeCurrent(Scene scene)
        {
            if (scene == null) throw new ArgumentNullException("Scene is null.");
            if(!_scenes.Contains(scene)) throw new ArgumentException("Scene not found.");
            if (scene == Current) return;
            if (Current != null) Current.DoDeactivate();
            Current = scene;
            scene.DoActivate();
        }

        public void Remove(Scene scene){
            if (scene == null) throw new ArgumentNullException("Scene is null.");
            if (!_scenes.Contains(scene)) throw new ArgumentException("Scene not found.");
            scene.Manager = null;
            if (Current == scene) scene.DoDeactivate();
            _scenes.Remove(scene);
        }
    }
}
