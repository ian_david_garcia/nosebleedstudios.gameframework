﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NosebleedStudios.GameFramework
{

    [Serializable]
    public sealed class Entity: MessageManager
    {
        #region Methods

        public Entity(string name)
        {
            Name = name;
        }

        public Entity(string name, params Component[] component) : this(name) {
            AddComponent(component);
        }

        public T GetComponent<T>()
        {
            return _components.OfType<T>().FirstOrDefault();
        }

        public IEnumerable<T> GetComponents<T>(bool includeChildren = false)
        {
            foreach (var cmp in _components.OfType<T>())
                yield return cmp;
            if (!includeChildren) yield break;
            foreach (var cmp in _children.SelectMany(c => c.GetComponents<T>(includeChildren)))
                yield return cmp;
        }

        public void AddComponent(params Component[] component)
        {
            foreach (var c in component)
            {
                if (c.Entity != null) throw new ArgumentException("Component already added to an entity.");
                c._componentNode = _components.AddLast(c);
                c.Entity = this;
                if (IsStarted) c.Start();
            }
        }

        public IEnumerable<Entity> GetAncestors() {
            if (Parent == null) yield break;
            yield return Parent;
            foreach (var p in Parent.GetAncestors())
                yield return p;
        }      

        public void AddChild(params Entity[] child)
        {
            foreach (var c in child)
            {
                if (c.Parent != null) throw new ArgumentException("Entity already has a parent.");
                c._parentNode = _children.AddLast(c);
                c.SetSceneRecursive(Scene);
                c.Parent = this;
                if (IsStarted) c.Start();
            }
            SendMessage(this, Messages.Framework.ChildAdded, Message.CreateDataMessage(child));
        }

        public void RemoveChild(Entity c)
        {
            if (c.Parent != this) throw new ArgumentException("Entity is not a child of this object.");
            if (c.IsStarted) c.Stop();
            _children.Remove(c._parentNode);
            c._parentNode = null;
            c.Parent = null;
            c.SetSceneRecursive(null);
            SendMessage(this, Messages.Framework.ChildRemoved, Message.CreateDataMessage(c));
        }

        public Entity FindChild(string name)
        {
            if (Name == name) return this;
            foreach (var c in _children) {
                var e = c.FindChild(name);
                if (e != null) return e;
            }
            return null;
        }

        public void BroadcastMessage(object source, int messageId, Message messageData=null)
        {
            SendMessage(source, messageId, messageData);
            foreach (var child in Children)
                child.BroadcastMessage(source, messageId, messageData);
        }

        #endregion

        #region Internal Methods

        internal void Start()
        {
            if (IsStarted) throw new InvalidOperationException("Entity already started.");
            
            var compNode = _components.First;
            while (compNode != null) {
                var thisNode = compNode;
                thisNode.Value.Start();
                compNode = compNode.Next;
            }

            var childNode = _children.First;
            while (childNode != null)
            {
                var thisNode = childNode;
                thisNode.Value.Start();
                childNode = childNode.Next;
            }
            IsStarted = true;
        }

        internal void Stop()
        {
            if (!IsStarted) throw new InvalidOperationException("Entity not started.");

            var compNode = _components.First;
            while (compNode != null)
            {
                var thisNode = compNode;
                compNode = compNode.Next;
                thisNode.Value.Stop();
            }

            var childNode = _children.First;
            while (childNode != null)
            {
                var thisNode = compNode;
                childNode = childNode.Next;
                thisNode.Value.Stop();
            }
            IsStarted = false;
        }

        internal void SetSceneRecursive(Scene s)
        {
            Scene = s;
            foreach (var c in _children)
            {
                Scene = s;
                c.SetSceneRecursive(s);
            }
        }

        #endregion

        #region Properties

        public string Name { get; set; }
        public IEnumerable<Component> Components { get { return _components; } }
        public IEnumerable<Entity> Children { get { return _children; } }
        public Entity Parent { get; private set; }
        public bool IsStarted { get; private set; }
        public Scene Scene { get; internal set; }
        public MessageManager Messaging { get { return _messaging; } }
        #endregion

        #region Private Members

        private LinkedList<Entity> _children = new LinkedList<Entity>();
        internal LinkedList<Component> _components = new LinkedList<Component>();
        private LinkedListNode<Entity> _parentNode;
        [NonSerialized]
        private MessageManager _messaging = new MessageManager();
        #endregion
    }
}
